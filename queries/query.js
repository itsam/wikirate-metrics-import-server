var async = require('async');
var Company = require('../models/company');
var Snippet = require('../models/snippet');
var csv = require('../custom_modules/csv');

var queries = {};

queries.getMetrics = function (metricName, sourceName) {

    var queryObject = {$and: [{'name': metricName}, {'source_name': sourceName} ] };

    Snippet.find(queryObject, function(err, snippets){

        if (err){

            console.log('Error in fetching snippets for metric ' + metricName);
            return err;
        } 

        getCompanies(metricName, snippets);
    });
};
    
var getCompanies = function (metricName, snippets){

    if (snippets && snippets.length > 0){

        var metrics = [];

        async.each(snippets, function(snippet, callback) {

            getCompanyById(snippet, metricName, metrics, callback);
        }, function(err){
            
            if (err) {
              
                console.log('Companies iteration failed.');
            } else {

                console.log('Companies iteration completed successfully, about to create csv file.');
                csv.createCsvFile(metrics, metricName);
            }
        });
    }
}

var getCompanyById = function(snippet, metricName, metrics, callback){

    if (snippet.value && snippet.referred_Company){

        var companyMetric = {};

        companyMetric.Year = snippet.citeyear;
        companyMetric.Value = snippet.value;
        companyMetric.Source = snippet.source;

        Company.findById(snippet.referred_Company, function (err, company){

            if (err){

                console.log('Error fetching company with ID ' + company.wikirate_company_name);
            } else {

                if (company.WikiRateID){

                    console.log('Company ' + company.wikirate_company_name + ' source name ' + snippet.source_name);
                    var author = 'Pantelis Ieronimakis + ';
                    companyMetric.Metric = author + metricName;     
                    companyMetric.Company = company.wikirate_company_name;
                    metrics.push(companyMetric);
                }
            }
            callback();
        });
    } else {

        callback();
    }
}

module.exports = queries;
