var mongoose = require('mongoose');

var snippetSchema = mongoose.Schema({

	name : String,
	value : Number,
	source: String,
	source_name: String,
	citeyear: Number, 
	referred_Company: { type : mongoose.Schema.Types.ObjectId , ref: 'Company'},	
	type: String,
	currency: String
});

module.exports = mongoose.model('Snippet', snippetSchema, 'Snippets');