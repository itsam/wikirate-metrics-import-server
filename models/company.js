var mongoose = require('mongoose');

var companySchema = mongoose.Schema({

	Company_name : String,
	Company_Link : String,
	Country: String,
	Logo: String, 
	Sector: String, 
	WikiRateID: Number,
	wikirate_company_name: String
});

module.exports = mongoose.model('Company', companySchema, 'Companies');