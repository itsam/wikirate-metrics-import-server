# README #

WikiRate metrics importing server.
==================================

Building & Running the Server
-----------------------------
### Prerequisites
[Node.js](https://nodejs.org/)  
[Node Foreman](https://github.com/strongloop/node-foreman) to read your environment (.env file must be filled) and start Node server  

### Start the server
Open your terminal and navigate to the project root folder. Run the following commands:
```
npm install
nf start
```

-------
Built using [Node.js](https://nodejs.org/) and [Express](http://expressjs.com/) framework.
