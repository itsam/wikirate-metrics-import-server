var mongoose = require('mongoose');
var Snippet = require('../models/snippet');

module.exports = function(){

	mongoose.connect(process.env.DATABASE_URI);

	mongoose.connection.on('error', function (err) {
		err.status = 504;  
		console.log('-----------------');
	 	console.log('Error! DB Connection failed, error status : ' + err.status);
	 	console.log('-----------------');
		return err;
	});

	mongoose.connection.once('open', function () {

		console.log('-------------------');
		console.log('DB Connection open!');
		console.log('-------------------');
	});

	return mongoose.connection;
}
