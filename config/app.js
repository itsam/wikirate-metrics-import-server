var express = require('express');

//	Open database.
var db = require('./db')();

var app = express();
var server = app.listen(process.env.PORT);

var queries = require('../queries/query');

var metricName = 'Net Income';
var sourceName = 'SEC';

queries.getMetrics(metricName, sourceName);